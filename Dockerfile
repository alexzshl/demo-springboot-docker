# kubesphere 镜像专用

FROM java:8
VOLUME /tmp
VOLUME /upload
VOLUME /logs
EXPOSE 8080
ADD './app.jar' '/www.jar'
RUN echo hello
#RUN bash -c 'echo world'
#RUN mkdir /test
#WORKDIR /
#ENTRYPOINT ["java","-D","-jar","/www.jar","--spring.profile.active=test"]
ENTRYPOINT ["java","-jar","/www.jar","-D","--spring.config.location=/config/spring/","--spring.profiles.active=kube","--server.port=8080"]
# 维护者
MAINTAINER hs
