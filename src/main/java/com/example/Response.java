package com.example;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author ZhengDing
 * @date 2022/4/16 22:49
 */
@Data
@NoArgsConstructor
// @AllArgsConstructor
public class Response<T> implements Serializable {
    
    private static final String SUCCESS_CODE = "0";
    private static final String ERROR_CODE = "500";
    
    private String code;
    private String msg;
    private T data;
    
    public Response(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    
    public static <T> Response<T> SUCCESS(String msg) {
        return new Response(SUCCESS_CODE, msg, null);
    }
    
    public static <T> Response<T> SUCCESS(String msg, T data) {
        return new Response<>(SUCCESS_CODE, msg, data);
    }
    
    public static <T> Response<T> ERROR(String msg) {
        return new Response<>(ERROR_CODE, msg, null);
    }
    
    public static <T> Response<T> ERROR(String code, String msg) {
        return new Response<>(code, msg, null);
    }
}
