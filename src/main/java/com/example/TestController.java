package com.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ZhengDing
 * @date 2022/4/16 22:48
 */
@RestController
@Slf4j
public class TestController {
    
    @GetMapping("test")
    public Response test(String msg) {
        return Response.SUCCESS(msg);
    }
}
